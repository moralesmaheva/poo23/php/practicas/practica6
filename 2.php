<?php
//programa que coloca dos numeros en las variables y realiza operaciones
$a = mt_rand(1, 100);
$b = mt_rand(1, 100);

//procesamiento
$suma = $a + $b;
$resta = $a - $b;
$multiplicacion = $a * $b;
$division = $a / $b;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <h1>Numero1: <?= $a ?></h1>
    <h1>Numero2: <?= $b ?></h1>
    <ul>
        <li>Suma: <?= $suma ?></li>
        <li>Producto: <?= $multiplicacion ?></li>
        <li>Resta: <?= $resta ?></li>
        <li>Cociente: <?= $division ?></li>
    </ul>
</body>

</html>