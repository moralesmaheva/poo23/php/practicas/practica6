<?php
//programa que coloca dos numeros en variables y realiza operaciones con ellos
$a = mt_rand(1, 100);
$b = mt_rand(1, 100);

//procesamiento
$suma = $a + $b;
$resta = $a - $b;
$multiplicacion = $a * $b;
$division = $a / $b;
$raiz = sqrt($a);
$potencia = $a ** $b;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>

<body>
    <ul>
        <li>La suma de los dos numeros es <?= $suma ?></li>
        <li>La resta de los dos numeros es <?= $resta ?></li>
        <li>La multiplicacion de los dos numeros es <?= $multiplicacion ?></li>
        <li>La division de los dos numeros es <?= $division ?></li>
        <li>La raiz cuadrada del primer numero es <?= $raiz ?></li>
        <li>El primer numero elevado al segundo es <?= $potencia ?></li>
    </ul>

</body>

</html>