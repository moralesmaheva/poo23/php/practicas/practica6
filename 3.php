<?php
//asignar un numero a una variable y mostrar si el numero es par o impar
$num = mt_rand(1, 100);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>
    <?php
    //procesamiento e impresion

    if (($num % 2) == 0) {
        echo "El numero {$num} es par";
    } else {
        echo "El numero {$num} es impar";
    };
    ?>
</body>

</html>