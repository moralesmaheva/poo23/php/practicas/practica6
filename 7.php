<?php
//programa que crea tres variables numericas e indica cual es el mayor
$a = mt_rand(1, 100);
$b = mt_rand(1, 100);
$c = mt_rand(1, 100);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>

<body>
    <ul>
        <li>El primer numero es <?= $a ?></li>
        <li>El segundo numero es <?= $b ?></li>
        <li>El tercer numero es <?= $c ?></li>
    </ul>
    <br>
    <?php
    //procesamiento e impresion
    if ($a > $b & $a > $c) {
        echo "{$a} es el numero mayor";
    } elseif ($b > $a & $b > $c) {
        echo "{$b} es el numero mayor";
    } else {
        echo "{$c} es el numero mayor";
    }
    ?>
</body>

</html>