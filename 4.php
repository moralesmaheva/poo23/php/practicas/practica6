<?php
//asignar un numero a una variable y mostrar si el numero es par o impar
$num = mt_rand(1, 10);
$par = "par";
$impar = "impar";

//procesamiento 
if ($num % 2 == 0) {
    $num = $par;
} else {
    $num = $impar;
};
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <div>El numero es <?= $num ?></div>
</body>

</html>