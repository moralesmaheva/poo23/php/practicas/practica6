<?php
//programa que nos diga que numero es mayor dentro de un array
$numeros = [mt_rand(1, 100), mt_rand(1, 100), mt_rand(1, 100)];
//la funcion max() muestra el numero mayor dentro del array
$mayor = max($numeros);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8</title>
</head>

<body>
    <div>El numero myor es <?= $mayor ?></div>
</body>

</html>