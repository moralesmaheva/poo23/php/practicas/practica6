<?php
//crear dos variables y determinar si son iguales o cual es mayor
$x = 1;
$y = 3;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <?php
    //procesamiento

    if ($x > $y) {
        echo "El mayor es {$x}";
    } elseif ($x == $y) {
        echo "{$x} y {$y} son iguales";
    } else {
        echo "El mayor es {$y}";
    }
    ?>
</body>

</html>